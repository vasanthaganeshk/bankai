# bankai

Scheme based programming language with static typing and type inference.

## Instructions to run:
+ Install stack tool (For haskell)
+ Install all the dependencies
+ **stack build**
+ **stack exec bankai-exe** (tutorial [here](https://docs.haskellstack.org/en/stable/README/))
