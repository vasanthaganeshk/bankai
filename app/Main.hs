module Main where

import Prelude hiding (lookup)
import Lib
import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Data.Either
import Control.Monad
import Data.Map

symbol :: Parser Char
symbol = oneOf "+-*/!%&#"

spaces :: Parser ()
spaces = skipMany1 space

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool
             deriving Show

parseString :: Parser LispVal
parseString = do
                char '"'
                x <- many (noneOf "\"")
                char '"'
                return $ String x

-- parseBool :: String -> Parser LispVal

-- parseBool = liftM

-- parseAtom :: Parser LispVal
-- parseAtom = do
--               first <- letter <|> symbol
--               rest  <- many (letter <|> digit <|> symbol)
--               return $ Atom (first:rest)

parseAtomOrBool :: Parser LispVal
parseAtomOrBool = do
              first <- letter <|> symbol
              rest  <- many (letter <|> digit <|> symbol)
              return $ case (first:rest) of
                         "#t" -> Bool True
                         "#f" -> Bool False
                         _    -> Atom (first:rest)

parseNumber :: Parser LispVal
parseNumber = liftM (Number . read) $ many1 digit

-- parseNumber2 :: Parser LispVal
-- parseNumber2 = do
--                  x <- many1 digit
--                  return $ (Number . read) x

parseList :: Parser LispVal
parseList = liftM List $ sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
                    head <- endBy parseExpr spaces
                    tail <- char '.' >> spaces >> parseExpr
                    return $ DottedList head tail

parseQuoted :: Parser LispVal
parseQuoted = do
                x <- char '\'' >> parseExpr
                return $ List [Atom "quote", x]

parseExpr :: Parser LispVal
parseExpr =  parseAtomOrBool
           <|> parseString
           <|> parseNumber
           <|> parseQuoted
           <|> do
                 char '('
                 x <- try parseList <|> parseDottedList
                 char ')'
                 return x

readExpr :: String -> LispVal
-- readExpr input
--   | isLeft val = String $ "No match\n" ++ show val ++ "\n"
--   | otherwise  = fromRight val
--   where val = parse parseExpr "lisp" input

readExpr input = case parse parseExpr "lisp" input of
                   Right val -> val
                   Left  err -> String $ "No match\n" ++ show err ++ "\n"

eval :: LispVal -> LispVal
eval val@(Number _) = val
eval val@(String _) = val
eval val@(Bool _)   = val
eval (List [Atom "quote", val]) = val
eval (List (Atom func:args)) = apply func $ Prelude.map eval args

primitives = insert "+" (numericBinOP (+))        .
             insert "-" (numericBinOP (-))        .
             insert "*" (numericBinOP (*))        .
             insert "%" (numericBinOP (mod))      .
             insert "/" (numericBinOP (div))      $
             empty

numericBinOP :: (Integer -> Integer -> Integer) -> [LispVal] -> LispVal
numericBinOP op args = Number $ foldl1 op $ Prelude.map unpackNum args

unpackNum :: LispVal -> Integer
unpackNum (Number n) = n
unpackNum _ = 0

apply :: String -> [LispVal] -> LispVal
apply func args = maybe (Bool False) ($ args) $ lookup func primitives

main :: IO ()
main = interact $ show . eval . readExpr
